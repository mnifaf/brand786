const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .vue()
    .sass('resources/sass/app.scss', 'public/css')
    .styles(['resources/css/app.css'],'public/css/custom.css');

mix.babelConfig({
    plugins: ['@babel/plugin-syntax-dynamic-import'],
});

// mix.js('resources/js/app.js', 'public/js')
//     .vue()
//     .sass('resources/assets/sass/app.scss', 'public/css');


// mix.webpackConfig({
//     devServer: {
//         proxy: {
//             '*': 'http://localhost:8000'
//         }
//     }
// });
// mix.options({
//     hmrOptions: {
//         host: 'localhost',
//         port: '8080'
//     }
// });
// if (mix.inProduction()) {
//     mix.version();
// }
