(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_js_vue_components_todoCreate_vue"],{

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/vue/components/todoCreate.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/vue/components/todoCreate.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _services_apiRequest__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../services/apiRequest */ "./resources/js/services/apiRequest.js");
/* harmony import */ var _services_browserStorage__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/browserStorage */ "./resources/js/services/browserStorage.js");


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  name: "todoCreate",
  data: function data() {
    return {
      title: "",
      description: "",
      showLoader: false,
      showErrorDiv: false,
      errMsg: "",
      showSuccessDiv: false
    };
  },
  methods: {
    createTodo: function () {
      var _createTodo = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().mark(function _callee() {
        var response;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                if (!(!this.title || !this.description)) {
                  _context.next = 5;
                  break;
                }

                this.showErrorDiv = true;
                this.showSuccessDiv = false;
                this.errMsg = "Field Required";
                return _context.abrupt("return");

              case 5:
                this.showLoader = true;
                _context.prev = 6;
                _context.next = 9;
                return _services_apiRequest__WEBPACK_IMPORTED_MODULE_1__.default.post('/api/todo', {
                  title: this.title,
                  description: this.description
                });

              case 9:
                response = _context.sent;

                if (response.data.status === true) {
                  this.showLoader = false;
                  this.showErrorDiv = false;
                  this.showSuccessDiv = true;
                  this.title = this.description = ""; // this will remove cache from indexedDB of browser storage

                  _services_browserStorage__WEBPACK_IMPORTED_MODULE_2__.default.localForage.clearAll();
                }

                _context.next = 19;
                break;

              case 13:
                _context.prev = 13;
                _context.t0 = _context["catch"](6);
                this.showLoader = true;
                this.showSuccessDiv = false;
                this.showErrorDiv = true;
                this.errMsg = _context.t0.data.msg || "Something went wrong!";

              case 19:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this, [[6, 13]]);
      }));

      function createTodo() {
        return _createTodo.apply(this, arguments);
      }

      return createTodo;
    }()
  }
});

/***/ }),

/***/ "./resources/js/vue/components/todoCreate.vue":
/*!****************************************************!*\
  !*** ./resources/js/vue/components/todoCreate.vue ***!
  \****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _todoCreate_vue_vue_type_template_id_364447b8_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./todoCreate.vue?vue&type=template&id=364447b8&scoped=true& */ "./resources/js/vue/components/todoCreate.vue?vue&type=template&id=364447b8&scoped=true&");
/* harmony import */ var _todoCreate_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./todoCreate.vue?vue&type=script&lang=js& */ "./resources/js/vue/components/todoCreate.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__.default)(
  _todoCreate_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__.default,
  _todoCreate_vue_vue_type_template_id_364447b8_scoped_true___WEBPACK_IMPORTED_MODULE_0__.render,
  _todoCreate_vue_vue_type_template_id_364447b8_scoped_true___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  "364447b8",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/vue/components/todoCreate.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/vue/components/todoCreate.vue?vue&type=script&lang=js&":
/*!*****************************************************************************!*\
  !*** ./resources/js/vue/components/todoCreate.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_todoCreate_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./todoCreate.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/vue/components/todoCreate.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_todoCreate_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__.default); 

/***/ }),

/***/ "./resources/js/vue/components/todoCreate.vue?vue&type=template&id=364447b8&scoped=true&":
/*!***********************************************************************************************!*\
  !*** ./resources/js/vue/components/todoCreate.vue?vue&type=template&id=364447b8&scoped=true& ***!
  \***********************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_todoCreate_vue_vue_type_template_id_364447b8_scoped_true___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_todoCreate_vue_vue_type_template_id_364447b8_scoped_true___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_todoCreate_vue_vue_type_template_id_364447b8_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./todoCreate.vue?vue&type=template&id=364447b8&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/vue/components/todoCreate.vue?vue&type=template&id=364447b8&scoped=true&");


/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/vue/components/todoCreate.vue?vue&type=template&id=364447b8&scoped=true&":
/*!**************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/vue/components/todoCreate.vue?vue&type=template&id=364447b8&scoped=true& ***!
  \**************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { attrs: { id: "login" } }, [
    _c("h3", { staticClass: "text-center text-white pt-5" }, [_vm._v(" ")]),
    _vm._v(" "),
    _c("div", { staticClass: "container" }, [
      _c(
        "div",
        {
          staticClass: "row justify-content-center align-items-center",
          attrs: { id: "login-row" }
        },
        [
          _c(
            "div",
            { staticClass: "col-md-6", attrs: { id: "login-column" } },
            [
              _c(
                "div",
                {
                  staticClass: "alert alert-danger",
                  class: _vm.showErrorDiv ? "" : "hidden",
                  attrs: { role: "alert" }
                },
                [_vm._v(_vm._s(_vm.errMsg))]
              ),
              _vm._v(" "),
              _c(
                "div",
                {
                  staticClass: "alert alert-success",
                  class: _vm.showSuccessDiv ? "" : "hidden",
                  attrs: { role: "alert" }
                },
                [
                  _vm._v(
                    "\n                    Successfully Created New Todo. "
                  ),
                  _c(
                    "router-link",
                    { staticClass: "text-info", attrs: { to: "/todo-list" } },
                    [_vm._v("Go Todo List")]
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "col-md-12", attrs: { id: "login-box" } },
                [
                  _c("div", {
                    staticClass: "lds-dual-ring overlay",
                    class: _vm.showLoader ? "" : "hidden",
                    attrs: { id: "loader" }
                  }),
                  _vm._v(" "),
                  _c(
                    "form",
                    {
                      staticClass: "form",
                      attrs: { id: "login-form", action: "", method: "post" }
                    },
                    [
                      _c("h3", { staticClass: "text-center " }, [
                        _vm._v("Create Todo")
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group" }, [
                        _c("label", { attrs: { for: "title" } }, [
                          _vm._v("Title:")
                        ]),
                        _c("br"),
                        _vm._v(" "),
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.title,
                              expression: "title"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: {
                            type: "text",
                            name: "title",
                            id: "title",
                            placeholder: "Title"
                          },
                          domProps: { value: _vm.title },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.title = $event.target.value
                            }
                          }
                        })
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group" }, [
                        _c("label", { attrs: { for: "description" } }, [
                          _vm._v("Description:")
                        ]),
                        _c("br"),
                        _vm._v(" "),
                        _c("textarea", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.description,
                              expression: "description"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: {
                            name: "description",
                            id: "description",
                            placeholder: "Description"
                          },
                          domProps: { value: _vm.description },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.description = $event.target.value
                            }
                          }
                        })
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group" }, [
                        _c("input", {
                          staticClass: "btn btn-info btn-md",
                          attrs: {
                            type: "button",
                            name: "submit",
                            value: "Create New Todo"
                          },
                          on: { click: _vm.createTodo }
                        })
                      ]),
                      _vm._v(" "),
                      _c(
                        "div",
                        {
                          staticClass: "text-right",
                          attrs: { id: "register-link" }
                        },
                        [
                          _c("router-link", { attrs: { to: "/todo-list" } }, [
                            _vm._v("Todo List")
                          ])
                        ],
                        1
                      )
                    ]
                  )
                ]
              )
            ]
          )
        ]
      )
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ })

}]);