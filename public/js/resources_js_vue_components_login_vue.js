(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_js_vue_components_login_vue"],{

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/vue/components/login.vue?vue&type=script&lang=js&":
/*!****************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/vue/components/login.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _utils_setAuthorizationTokenHeader__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../utils/setAuthorizationTokenHeader */ "./resources/js/utils/setAuthorizationTokenHeader.js");
/* harmony import */ var _services_browserStorage__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/browserStorage */ "./resources/js/services/browserStorage.js");
/* harmony import */ var _utils_globalUtilities__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../utils/globalUtilities */ "./resources/js/utils/globalUtilities.js");
/* harmony import */ var _services_apiRequest__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/apiRequest */ "./resources/js/services/apiRequest.js");


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  name: "login",
  data: function data() {
    return {
      email: "",
      password: "",
      showLoader: false,
      showErrorDiv: false,
      errMsg: ""
    };
  },
  methods: {
    showError: function showError(msg) {
      this.showErrorDiv = true;
      this.errMsg = "".concat(msg);
    },
    login: function () {
      var _login = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().mark(function _callee() {
        var response, token;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                if (_utils_globalUtilities__WEBPACK_IMPORTED_MODULE_3__.default.isValidEmail(this.email)) {
                  _context.next = 3;
                  break;
                }

                this.showError("Required valid Email field");
                return _context.abrupt("return");

              case 3:
                if (this.password.trim()) {
                  _context.next = 6;
                  break;
                }

                this.showError("Password field required");
                return _context.abrupt("return");

              case 6:
                this.showLoader = true;
                _context.prev = 7;
                _context.next = 10;
                return _services_apiRequest__WEBPACK_IMPORTED_MODULE_4__.default.post('/api/login', {
                  email: this.email,
                  password: this.password
                });

              case 10:
                response = _context.sent;

                if (!(response.data.status === true)) {
                  _context.next = 20;
                  break;
                }

                token = response.data.token;
                (0,_utils_setAuthorizationTokenHeader__WEBPACK_IMPORTED_MODULE_1__.default)(token);
                _context.next = 16;
                return _services_browserStorage__WEBPACK_IMPORTED_MODULE_2__.default.cookieStorage.set('token', token);

              case 16:
                this.showLoader = false;
                this.$store.commit('setLoginState', true);
                this.$store.commit('setEmail', response.data.user.email);
                this.$router.push('/todo-list');

              case 20:
                _context.next = 27;
                break;

              case 22:
                _context.prev = 22;
                _context.t0 = _context["catch"](7);
                this.showLoader = false;
                this.showErrorDiv = true;
                this.errMsg = _context.t0.data.msg || "Something went wrong!";

              case 27:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this, [[7, 22]]);
      }));

      function login() {
        return _login.apply(this, arguments);
      }

      return login;
    }()
  }
});

/***/ }),

/***/ "./resources/js/vue/components/login.vue":
/*!***********************************************!*\
  !*** ./resources/js/vue/components/login.vue ***!
  \***********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _login_vue_vue_type_template_id_eaf4bcd2_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./login.vue?vue&type=template&id=eaf4bcd2&scoped=true& */ "./resources/js/vue/components/login.vue?vue&type=template&id=eaf4bcd2&scoped=true&");
/* harmony import */ var _login_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./login.vue?vue&type=script&lang=js& */ "./resources/js/vue/components/login.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__.default)(
  _login_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__.default,
  _login_vue_vue_type_template_id_eaf4bcd2_scoped_true___WEBPACK_IMPORTED_MODULE_0__.render,
  _login_vue_vue_type_template_id_eaf4bcd2_scoped_true___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  "eaf4bcd2",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/vue/components/login.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/vue/components/login.vue?vue&type=script&lang=js&":
/*!************************************************************************!*\
  !*** ./resources/js/vue/components/login.vue?vue&type=script&lang=js& ***!
  \************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_login_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./login.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/vue/components/login.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_login_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__.default); 

/***/ }),

/***/ "./resources/js/vue/components/login.vue?vue&type=template&id=eaf4bcd2&scoped=true&":
/*!******************************************************************************************!*\
  !*** ./resources/js/vue/components/login.vue?vue&type=template&id=eaf4bcd2&scoped=true& ***!
  \******************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_login_vue_vue_type_template_id_eaf4bcd2_scoped_true___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_login_vue_vue_type_template_id_eaf4bcd2_scoped_true___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_login_vue_vue_type_template_id_eaf4bcd2_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./login.vue?vue&type=template&id=eaf4bcd2&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/vue/components/login.vue?vue&type=template&id=eaf4bcd2&scoped=true&");


/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/vue/components/login.vue?vue&type=template&id=eaf4bcd2&scoped=true&":
/*!*********************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/vue/components/login.vue?vue&type=template&id=eaf4bcd2&scoped=true& ***!
  \*********************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { attrs: { id: "login" } }, [
    _c("h3", { staticClass: "text-center text-white pt-5" }, [_vm._v(" ")]),
    _vm._v(" "),
    _c("div", { staticClass: "container" }, [
      _c(
        "div",
        {
          staticClass: "row justify-content-center align-items-center",
          attrs: { id: "login-row" }
        },
        [
          _c(
            "div",
            { staticClass: "col-md-6", attrs: { id: "login-column" } },
            [
              _c(
                "div",
                {
                  staticClass: "alert alert-danger",
                  class: _vm.showErrorDiv ? "" : "hidden",
                  attrs: { id: "errorDiv", role: "alert" }
                },
                [_vm._v(_vm._s(_vm.errMsg))]
              ),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "col-md-12", attrs: { id: "login-box" } },
                [
                  _c("div", {
                    staticClass: "lds-dual-ring overlay",
                    class: _vm.showLoader ? "" : "hidden",
                    attrs: { id: "loader" }
                  }),
                  _vm._v(" "),
                  _c(
                    "form",
                    {
                      staticClass: "form",
                      attrs: { id: "login-form", action: "", method: "post" }
                    },
                    [
                      _c("h3", { staticClass: "text-center" }, [
                        _vm._v("Login")
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group" }, [
                        _c("label", { attrs: { for: "username" } }, [
                          _vm._v("Email:")
                        ]),
                        _c("br"),
                        _vm._v(" "),
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.email,
                              expression: "email"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: {
                            type: "text",
                            name: "username",
                            id: "username",
                            placeholder: "Email"
                          },
                          domProps: { value: _vm.email },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.email = $event.target.value
                            }
                          }
                        })
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group" }, [
                        _c("label", { attrs: { for: "password" } }, [
                          _vm._v("Password:")
                        ]),
                        _c("br"),
                        _vm._v(" "),
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.password,
                              expression: "password"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: {
                            type: "password",
                            name: "password",
                            id: "password",
                            placeholder: "Password"
                          },
                          domProps: { value: _vm.password },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.password = $event.target.value
                            }
                          }
                        })
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group" }, [
                        _c("input", {
                          staticClass: "btn btn-info btn-md",
                          attrs: {
                            type: "button",
                            name: "submit",
                            value: "Login"
                          },
                          on: { click: _vm.login }
                        })
                      ]),
                      _vm._v(" "),
                      _c(
                        "div",
                        {
                          staticClass: "text-right",
                          attrs: { id: "register-link" }
                        },
                        [
                          _c("router-link", { attrs: { to: "/register" } }, [
                            _vm._v("Register Here")
                          ])
                        ],
                        1
                      )
                    ]
                  )
                ]
              )
            ]
          )
        ]
      )
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ })

}]);