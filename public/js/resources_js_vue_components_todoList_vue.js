(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_js_vue_components_todoList_vue"],{

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/vue/components/todoList.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/vue/components/todoList.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _services_apiRequest__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../services/apiRequest */ "./resources/js/services/apiRequest.js");
/* harmony import */ var _services_browserStorage__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/browserStorage */ "./resources/js/services/browserStorage.js");


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  name: "todoList",
  data: function data() {
    return {
      showLoader: true,
      todoCollection: []
    };
  },
  methods: {
    deleteTodo: function deleteTodo(e) {
      var _this = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().mark(function _callee() {
        var response;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _this.showLoader = true;
                _context.prev = 1;
                _context.next = 4;
                return _services_apiRequest__WEBPACK_IMPORTED_MODULE_1__.default.delete("/api/todo/".concat(e.target.id));

              case 4:
                response = _context.sent;

                if (response.data.status) {
                  e.target.closest('tr').remove();
                  _this.showLoader = false;
                  _services_browserStorage__WEBPACK_IMPORTED_MODULE_2__.default.localForage.clearAll();
                }

                _context.next = 11;
                break;

              case 8:
                _context.prev = 8;
                _context.t0 = _context["catch"](1);
                _this.showLoader = false;

              case 11:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, null, [[1, 8]]);
      }))();
    }
  },
  mounted: function mounted() {
    var _this2 = this;

    try {
      //  'cache'  word mean this will check localstorage
      //  if not found data then make http request and save data into cache.
      //  Like indexedDB of browser by using localforge
      _services_apiRequest__WEBPACK_IMPORTED_MODULE_1__.default.cache.get('/api/todo').then(function (response) {
        _this2.todoCollection = response.data.data;
        _this2.showLoader = false;
      });
    } catch (e) {
      this.showLoader = false;
    }
  }
});

/***/ }),

/***/ "./resources/js/vue/components/todoList.vue":
/*!**************************************************!*\
  !*** ./resources/js/vue/components/todoList.vue ***!
  \**************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _todoList_vue_vue_type_template_id_2efeaec6_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./todoList.vue?vue&type=template&id=2efeaec6&scoped=true& */ "./resources/js/vue/components/todoList.vue?vue&type=template&id=2efeaec6&scoped=true&");
/* harmony import */ var _todoList_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./todoList.vue?vue&type=script&lang=js& */ "./resources/js/vue/components/todoList.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__.default)(
  _todoList_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__.default,
  _todoList_vue_vue_type_template_id_2efeaec6_scoped_true___WEBPACK_IMPORTED_MODULE_0__.render,
  _todoList_vue_vue_type_template_id_2efeaec6_scoped_true___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  "2efeaec6",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/vue/components/todoList.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/vue/components/todoList.vue?vue&type=script&lang=js&":
/*!***************************************************************************!*\
  !*** ./resources/js/vue/components/todoList.vue?vue&type=script&lang=js& ***!
  \***************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_todoList_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./todoList.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/vue/components/todoList.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_todoList_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__.default); 

/***/ }),

/***/ "./resources/js/vue/components/todoList.vue?vue&type=template&id=2efeaec6&scoped=true&":
/*!*********************************************************************************************!*\
  !*** ./resources/js/vue/components/todoList.vue?vue&type=template&id=2efeaec6&scoped=true& ***!
  \*********************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_todoList_vue_vue_type_template_id_2efeaec6_scoped_true___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_todoList_vue_vue_type_template_id_2efeaec6_scoped_true___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_todoList_vue_vue_type_template_id_2efeaec6_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./todoList.vue?vue&type=template&id=2efeaec6&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/vue/components/todoList.vue?vue&type=template&id=2efeaec6&scoped=true&");


/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/vue/components/todoList.vue?vue&type=template&id=2efeaec6&scoped=true&":
/*!************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/vue/components/todoList.vue?vue&type=template&id=2efeaec6&scoped=true& ***!
  \************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "container" }, [
    _c("div", { staticClass: "col-md-12" }, [
      _c("div", { staticClass: " d-flex justify-content-between mt-4" }, [
        _c("h5", { staticClass: "bg-light" }, [_vm._v("Your Todo List")]),
        _vm._v(" "),
        _c(
          "div",
          [
            _c(
              "router-link",
              {
                staticClass: " col-md-offset-3 mb-3 float-xl-right",
                attrs: { to: "/todo-create" }
              },
              [_vm._v("Create new Todo")]
            )
          ],
          1
        )
      ])
    ]),
    _vm._v(" "),
    _c("div", {
      staticClass: "alert alert-success",
      staticStyle: { display: "none" },
      attrs: { id: "successDiv", role: "alert" }
    }),
    _vm._v(" "),
    _c("div", { staticClass: "row col-md-12 col-md-offset-2 custyle" }, [
      _c("table", { staticClass: "table table-striped custab" }, [
        _c("div", {
          staticClass: "lds-dual-ring overlay",
          class: _vm.showLoader ? "" : "hidden",
          attrs: { id: "loader" }
        }),
        _vm._v(" "),
        _vm._m(0),
        _vm._v(" "),
        _c(
          "tbody",
          _vm._l(_vm.todoCollection, function(obj, key) {
            return _c("tr", { key: key }, [
              _c("td", [_vm._v(_vm._s(obj.id))]),
              _vm._v(" "),
              _c("td", [_vm._v(_vm._s(obj.title))]),
              _vm._v(" "),
              _c("td", [_vm._v(_vm._s(obj.description))]),
              _vm._v(" "),
              _c(
                "td",
                { staticClass: "text-center" },
                [
                  _c(
                    "router-link",
                    {
                      staticClass: "btn btn-info btn-xs",
                      attrs: { to: /todo-update/ + obj.id }
                    },
                    [
                      _c("span", { staticClass: "glyphicon glyphicon-edit" }),
                      _vm._v(" Edit")
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "a",
                    {
                      staticClass: "btn btn-danger btn-xs",
                      attrs: { href: "javascript:void(0);", id: obj.id },
                      on: { click: _vm.deleteTodo }
                    },
                    [
                      _c("span", { staticClass: "glyphicon glyphicon-remove" }),
                      _vm._v(" Del")
                    ]
                  )
                ],
                1
              )
            ])
          }),
          0
        )
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", [
      _c("tr", [
        _c("th", [_vm._v("ID")]),
        _vm._v(" "),
        _c("th", [_vm._v("Title")]),
        _vm._v(" "),
        _c("th", [_vm._v("Parent ID")]),
        _vm._v(" "),
        _c("th", { staticClass: "text-center" }, [_vm._v("Action")])
      ])
    ])
  }
]
render._withStripped = true



/***/ })

}]);