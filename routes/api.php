<?php

    use App\Http\Controllers\Auth;
    use App\Http\Controllers\TodoController;
    use App\Http\Controllers\VerifyEmailController;
    use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::post('/signup', [Auth::class, 'signup']);
Route::post('/login', [Auth::class, 'login']);

Route::get('/email/verify/{id}/{hash}', [VerifyEmailController::class, '__invoke'])->name('verification.verify');
Route::get('/email/resend', [VerifyEmailController::class, 'resend'])->name('verification.resend');


Route::group(['middleware' => 'auth.jwt'], function () {
    Route::get('/profile', [Auth::class, 'getUser']);
    Route::post('/logout', [Auth::class, 'logout']);
    Route::resource('todo', TodoController::class, ['except' => ['create','edit']]);
});
