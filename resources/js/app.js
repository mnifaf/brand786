require('./bootstrap');
import Vue from 'vue';
import router from './router'
import App from './vue/app'
import Vuex from 'vuex'
// Store
Vue.use(Vuex)
const store = new Vuex.Store({
    state: {
        isLogin: false,
        email: 'Guest'
    },
    mutations: {
        setLoginState (state, login) {
            state.isLogin = login
        },
        setEmail (state, email) {
            state.email = email
        }
    }
})
const app = new Vue({
     el: '#app',
     router,
     store,
     components: { App }
 });
