export const BASE_URL_CORE = "https://2zn7zskbb4.execute-api.ap-south-1.amazonaws.com/dev/"
export const HTTP_METHOD = {
    POST:  "POST",
    GET:  "GET",
    PUT:  "PUT",
    DELETE:  "DELETE"
};
