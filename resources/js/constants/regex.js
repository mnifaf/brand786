export const EMAIL_REGEX = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/iu;
export const PASSWORD_REGEX = /^(?=.+\d)(?=.*[a-z])(?=.*[A-Z])(\S){8,25}$/u;
