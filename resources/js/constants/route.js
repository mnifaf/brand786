export const HOME_PAGE = "/";

export const LOGIN = "/a/login";
export const REGISTER = "/a/register";
export const FORGOT_PASSWORD = "/a/forgot-password";
export const LOGIN_HELP = "/HELP";

export const SPORTS_FOOTBALL = "/sports/football";
export const SPORTS_LEAGUES = "/sports/football/leagues";
export const SPORTS_CLUBS = "/sports/football/clubs";
export const SPORTS_ACADEMIES = "/sports/football/academies";
export const SPORTS_PLAYERS = "/sports/football/players";
export const SPORTS_NEWS = "/sports/football/news";

export const TERMS_CONDITIONS = "/TERMS_CONDITIONS";
export const PRIVACY_POLICY = "/PRIVACY_POLICY";
