let config = {
    // API_CACHE
    API_CACHE_DURATIONS : {
        SHORT:  ( 20 * 60), // 2 =  2min
        MEDIUM:  (45 * 60 ), // 45min
        LONG: (3 * 24 * 60 * 60 ) // 3 days
    },
    API_CACHE_ENABLE : true,  // if this is false then api will note save data into browser storage
    BASE_64_Enable_FOR_LOCAL_STORAGE   : true,
    BASE_64_Enable_FOR_SESSION_STORAGE : false,
};

export default config;
