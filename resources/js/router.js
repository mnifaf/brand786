import Vue from 'vue';
import VueRouter from 'vue-router';

import isTokenExist from './utils/isTokenExist'
Vue.use(VueRouter);
const routes = [
    {
        path: '/',
        component: () => import( './vue/components/welcome'),
        name: "welcome",
        beforeEnter: (to, from, next) => {
            if(isTokenExist()){
                next('/todo-list');
            }else{
                next();
            }
        }
    },
    {
        path: '/login',
        component: () => import('./vue/components/login'),
        beforeEnter: (to, from, next) => {
            if(isTokenExist()){
                next('/todo-list');
            }else{
                next();
            }
        }
    },
    {
        path: '/verify-email',
        component: () => import('./vue/components/verifyEmail'),
        beforeEnter: (to, from, next) => {
            if(isTokenExist()){
                next('/todo-list');
            }else{
                next();
            }
        }
    },
    {
        path: '/already-verify-email',
        component:  () => import('./vue/components/alreadyVerifyEmail'),
        beforeEnter: (to, from, next) => {
            if(isTokenExist()){
                next('/todo-list');
            }else{
                next();
            }
        }
    },
    {
        path: '/register',
        component: () => import('./vue/components/register'),
        beforeEnter: (to, from, next) => {
            if(isTokenExist()){
                next('/todo-list');
            }else{
                next();
            }
        }
    },
    {
        path: '/todo-create',
        component: () => import('./vue/components/todoCreate'),
        beforeEnter: (to, from, next) => {
            if(isTokenExist()){
                next();
            }else{
                next('/login');
            }
        }
    },
    {
        path: '/todo-list',
        component: () => import('./vue/components/todoList'),
        beforeEnter: (to, from, next) => {
            if(isTokenExist()){
                next();
            }else{
                next('/login');
            }
        }
    },
    {
        path: '/todo-update/:id?',
        component: () => import('./vue/components/todoUpdate'),
        beforeEnter: (to, from, next) => {
            if(!isTokenExist()){
                next();
            }else{
                next();
            }
        }
    }
]

const router = new VueRouter({mode: 'history', routes});
// router.beforeEach( (to, from, next) =>{
//     const token = localStorage.getItem("token") || null;
//     if(token){
//         window.axios.defaults.headers['Authorization'] = `Bearer ${token}`;
//     }
//     next();
// } )

export default router;
