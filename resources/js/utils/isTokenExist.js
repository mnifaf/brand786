import BrowserStorage from "../services/browserStorage";

let isTokenExist = () =>{
    let token = BrowserStorage.cookieStorage.get('token');
    return token || false;
}
export default isTokenExist;
