import {EMAIL_REGEX, PASSWORD_REGEX} from "../constants/regex";

let globalUtilities = (() => {
    return {
        cache : {
            isExpiredApi : (timestamp) => {
                if(timestamp){
                    let current_timestamp = ~~(Date.now() / 1000);
                    return current_timestamp > timestamp;
                }
            },
            currentTimeStampInSecond : (addSecond = undefined) => {
                let currentTimeStamp = ~~(Date.now() / 1000);   // ~~ means Math.floor;
                if(addSecond){
                    currentTimeStamp += (addSecond);
                }
                return currentTimeStamp;
            },
        },
        isValidEmail : (email) => {
           return (email.trim().match(EMAIL_REGEX));
        },
        isValidPassword : (password) => {
           return (password.trim().match(PASSWORD_REGEX));
        },
    }
    })();

    export default globalUtilities;
