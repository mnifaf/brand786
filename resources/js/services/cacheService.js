import BrowserStorage from './browserStorage'
import globalUtilites from '../utils/globalUtilities'

export default {
    async isCacheExists(apiName) {
        let dataFromCache = await BrowserStorage.localForage.getWithJsonStringify(apiName);
        if (dataFromCache && !globalUtilites.cache.isExpiredApi(dataFromCache.x)) {  //x === expireTime
            return { doFetch: false, savedData: dataFromCache.savedData }
        } else {
            return { doFetch: true ,savedData : {} }
        }
    },

    async createCache(apiName, response, expireTime ) {
        let currentTimeStamp = globalUtilites.cache.currentTimeStampInSecond(expireTime);
        let resWithCacheTime = { x: currentTimeStamp, savedData: response };
        BrowserStorage.localForage.setWithJsonStringify(apiName, resWithCacheTime);
    }
};
