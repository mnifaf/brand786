import localforage from "localforage"
import Cookies from "js-cookie";
import  config from "../config"

const { BASE_64_Enable_FOR_LOCAL_STORAGE , BASE_64_Enable_FOR_SESSION_STORAGE , BASE_64_Enable_FOR_INDEX_DB } = config;

let BrowserStorage = (() => {
    return {
        localForage : {
            set : async (key , value ) => {
                 localforage.setItem(key.trim() , value ).then(() => {
                    console.log("data saved with the name of " + key);
                 });
            },
            get : async key => { return localforage.getItem(key.trim()).then((data) => { return data });
            },
            setWithJsonStringify :  (key , value , decodeBase64 = true) => {
                let data = JSON.stringify(value);
                if(BASE_64_Enable_FOR_INDEX_DB && decodeBase64){
                    data = window.btoa(data);
                }
                localforage.setItem(key.trim() , data ).then(() => {
                    setTimeout(() => console.log("data saved with the name of "), 2000);

                 });
            },
            getWithJsonStringify : async (key , decodeBase64 = true) => {
                let data = await localforage.getItem(key.trim()).then((data) => { return data });
                if(data){
                    if(BASE_64_Enable_FOR_INDEX_DB && decodeBase64){
                        data = window.atob(data);
                    }
                    return JSON.parse(data);
                }else{
                    return data;
                }
            },
            clearAll : () => {
                localforage.clear()
            }
        },
        localStorage: {
            set :  (key , value , decodeBase64 = true) => {
                value =  (typeof value === 'string') ?  value.trim() : value;
                if(BASE_64_Enable_FOR_LOCAL_STORAGE && decodeBase64) {
                    value = window.btoa(value);
                }
                window.localStorage.setItem(key.trim(), value);
            },
            get : (key , decodeBase64 = true) => {
                let data =   window.localStorage.getItem(key.trim());
                if(BASE_64_Enable_FOR_LOCAL_STORAGE  && decodeBase64) {
                    data = window.atob(data);
                }
                return data;
            },
            setWithJsonStringify : (key , value , decodeBase64 = true) => {
                let data = JSON.stringify(value);
                if(BASE_64_Enable_FOR_LOCAL_STORAGE && decodeBase64){
                    data = window.btoa(data);
                }
                window.localStorage.setItem( key.trim(), data);
            },
            getWithJsonStringify : (key , decodeBase64 = true) => {
                let data = window.localStorage.getItem(key.trim());
                if(data){
                    if(BASE_64_Enable_FOR_LOCAL_STORAGE && decodeBase64){
                        data = window.atob(data);
                    }
                    return JSON.parse(data);
                }else{
                    return data;
                }
            },
            isKeyExists : (key) => {
                let newKey = key.trim();
                return (window.localStorage.getItem(newKey) !== null);
            },
            delete : {
                all :  () => {
                    window.localStorage.clear();
                },
                byKey : (key) => {
                    window.localStorage.removeItem(key.trim());
                }
            }
        },
        sessionStorage: {
            set :  (key , value , decodeBase64 = true) => {
                value =  (typeof value === 'string') ?  value.trim() : value;
                if(BASE_64_Enable_FOR_SESSION_STORAGE && decodeBase64) {
                    value = window.btoa(value);
                }
                window.sessionStorage.setItem(key.trim() , value );
            },
            get : (key , decodeBase64 = true) => {
                let data =  window.sessionStorage.getItem(key.trim());
                if( BASE_64_Enable_FOR_SESSION_STORAGE && decodeBase64 ) {
                    data = window.atob(data);
                }
                return data;
            },
            setWithJsonStringify : (key , value , decodeBase64 = true) => {
                let data =  JSON.stringify(value);
                if(BASE_64_Enable_FOR_SESSION_STORAGE && decodeBase64){
                    data = window.btoa(data);
                }
                window.sessionStorage.setItem(key.trim(), data);
            },
            getWithJsonStringify : (key , decodeBase64 = true) => {
                let data = window.sessionStorage.getItem(key.trim());
                if(data){
                    if(BASE_64_Enable_FOR_SESSION_STORAGE && decodeBase64){
                        data = window.atob(data);
                    }
                    return JSON.parse(data);
                }else{
                    return data;
                }
            },
            isKeyExists : (key) => {
                let newKey = key.trim();
                return (window.sessionStorage.getItem(newKey) !== null);
            },
            delete : {
                all : () => {
                    window.sessionStorage.clear();
                },
                byKey : (key) => {
                    window.sessionStorage.removeItem(key.trim());
                }
            }
        },
        cookieStorage : {
            set : (key , value) => {
                Cookies.set(key , value);
            },
            get : (key) => {
                return Cookies.get(key);
            },
            remove : (key) => {
                Cookies.remove(key);
                // return Cookies.set(key , '' , -1);
            },
            getWithJsonStringify : (key) => {
                let data = Cookies.get(key.trim());
                if(data){
                    return JSON.parse(data);
                }else{
                    return data;
                }
            },
            getCookieAtServerWithName : (key, req) => {
                if (!req.headers.cookie) {
                    return undefined;
                }
                const rawCookie = req.headers.cookie
                .split(';')
                .find(c => c.trim().startsWith(`${key}=`));
                if (!rawCookie) {
                    return undefined;
                }
                return rawCookie.split('=')[1];
            }
        }
    }
})();

export default BrowserStorage;
