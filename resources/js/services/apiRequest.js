import axios from "axios";
import cacheService from './cacheService'
import config from "../config"
import { HTTP_METHOD } from "../constants/constants"
// const globalHeaders = {Accept: 'application/json ; charset=UTF-8','Content-Type': 'application/json'}

const { API_CACHE_DURATIONS } = config;

async function apiRequestWithCache(url, cacheDuration, method , data = {}) {
    const dataFromCache = await cacheService.isCacheExists(url);
    const {doFetch , savedData} = dataFromCache;
    if (doFetch) {
       let response = await apiRequest(url , method , data);
       if(response.status === 200) {
           let { data } = response;
           let res = { status : response.status , data}
           cacheService.createCache(url , res ,  cacheDuration )
       }
        return response;
    } else {
        return savedData;
    }

}
async function apiRequest( url , method , data = {} ) {
        let rawResponse = '';
        try {
            switch (method) {
                case HTTP_METHOD.GET:
                    rawResponse = await axios.request({ url,  method });
                    return rawResponse;
                case HTTP_METHOD.DELETE:
                    rawResponse = await axios.request({ url,  method });
                    return rawResponse;
                case HTTP_METHOD.POST:
                    rawResponse = await axios.request({  url,  method , data });
                    return rawResponse;
                case HTTP_METHOD.PUT:
                    rawResponse = await axios.request({  url,  method , data });
                    return rawResponse;
                default:
                    console.log("Method not defined in api request")
            }
          }catch(error) {
            throw error.response
        }

}

const fetchResource = (() => {
    return {
            get : async (url) => {
                return apiRequest(url , HTTP_METHOD.GET);
            },
            post: async (url , data) => {
                return apiRequest(url, HTTP_METHOD.POST , data);
            },
            put: async (url , data) => {
                return apiRequest(url, HTTP_METHOD.PUT , data);
            },
            delete: async (url , data) => {
                return apiRequest(url, HTTP_METHOD.DELETE , data);
            },
        cache : {
            get : async (url, cacheDuration = API_CACHE_DURATIONS.SHORT) => {
                return apiRequestWithCache( url , cacheDuration , HTTP_METHOD.GET);
             },
             post: async (url , data , cacheDuration = API_CACHE_DURATIONS.SHORT ) => {
                return apiRequestWithCache(url , cacheDuration , HTTP_METHOD.POST , data);
             },
        }
    }
})();

export default fetchResource;
