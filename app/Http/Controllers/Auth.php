<?php

namespace App\Http\Controllers;
use App\Models\User;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Auth\Events\Registered;
use Tymon\JWTAuth\Facades\JWTAuth;

class Auth extends Controller
{
    public function signup(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'email' => 'required|email',
                'password' => 'required',
            ]);

        if ($validator->fails()) {
            return response()->json(['msg'=>$validator->errors()], 401);
        }
        try {
            $user = new User();
            $user->email = $request->email;
            $user->password = bcrypt($request->password);
            $user->save();
        }catch (QueryException  $ex){
            return response()->json([ 'status' => false,'msg'=>"User already exist"], Response::HTTP_CONFLICT);
        }
        // this will send an email
        event(new Registered($user));
        return response()->json([
            'status' => true,
            'msg' => "An email has been sent to you email address. Please verify your email.",
        ], Response::HTTP_OK);
    }

    public function login(Request $request)
    {
        try{
            $input = $request->only('email', 'password');
            if (!$jwt_token = JWTAuth::attempt($input)) {
                return response()->json([
                    'status' => false,
                    'msg' => 'Invalid Email or Password',
                ], Response::HTTP_UNAUTHORIZED);
            }
            if (empty(auth()->user()->email_verified_at))
            {
                return response()->json(['msg' => 'Your have not verified your email.'], Response::HTTP_NOT_FOUND);
            }
            return response()->json([
                'status' => true,
                'token' => $jwt_token,
                'user' => auth()->user()
            ]);
        }catch(JWTException $ex){
            return response()->json(['msg' => 'could not create token'], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

    }

    public function logout(Request $request)
    {
        try {
            JWTAuth::invalidate($request->token);

            return response()->json([
                'status' => true,
                'msg' => 'User logged out successfully'
            ]);
        } catch (JWTException $exception) {
            return response()->json([
                'status' => false,
                'msg' => 'Sorry, the user cannot be logged out'
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
    public function getUser(Request $request)
    {
//        $this->validate($request, [
//            'token' => 'required'
//        ]);
        $user = JWTAuth::authenticate($request->token);
        return response()->json(['user' => $user]);
    }

}
