<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Auth\Events\Verified;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;

class VerifyEmailController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api')->only('resend');
        $this->middleware('signed')->only('verify');
        $this->middleware('throttle:6,1')->only('verify', 'resend');
    }
    public function __invoke(Request $request): RedirectResponse
    {
        $user = User::find($request->route('id'));

        if ($user->hasVerifiedEmail()) {
            return redirect(env('FRONT_URL') . '/already-verify-email');
        }

        if ($user->markEmailAsVerified()) {
            event(new Verified($user));
        }
        return redirect(env('FRONT_URL') . '/verify-email');
    }

    public function resend(Request $request){
        if ($request->user()->hasVerifiedEmail()) {
            return response()->json(['msg'=> 'Already verified'], 401);
        }

        $request->user()->sendEmailVerificationNotification();

        if ($request->wantsJson()) {
            return response()->json(['msg'=> 'Email Send Successfully ', 'status' => true]);
        }

        return response()->json(['msg'=> 'Email Send Successfully ', 'status' => true]);

    }
}
