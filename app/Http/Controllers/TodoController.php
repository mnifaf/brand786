<?php

namespace App\Http\Controllers;

use App\Models\UserTodo;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use JWTAuth;
class TodoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        $userId =  $user->toArray()['id'];
        $allTodo = UserTodo::where('user_id', $userId)->orderBy('created_at', 'DESC')->get()->toArray();
        if($allTodo){
            return response()->json([
                'status' => true,
                'data' => $allTodo
            ], Response::HTTP_OK);
        }else{
            return response()->json([
                'status' => false,
                'data' => "No records found"
            ], Response::HTTP_NOT_FOUND);
        }
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $token = JWTAuth::getToken();
//        $user = JWTAuth::authenticate($token);
        $user = JWTAuth::toUser($token);
        try {
            $todo = new UserTodo();
            $todo->title = $request->title;
            $todo->description = $request->description;
            $todo->user_id = $user->toArray()['id'];
            $todo->save();
        }
        catch(QueryException $ex){
            return response()->json([
                'status' => false,
                'msg' => $ex->getMessage()
            ], Response::HTTP_CONFLICT);
        }
        return response()->json([
            'status' => true,
            'data' => $todo
        ], Response::HTTP_OK);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $allTodo = UserTodo::find($id);
        if($allTodo){
            return response()->json([
                'status' => true,
                'data' => $allTodo
            ], Response::HTTP_OK);
        }else{
            return response()->json([
                'status' => false,
                'msg' => 'Not record found'
            ], Response::HTTP_NOT_FOUND);
        }
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $todoFind = UserTodo::find($id);
        try {
            $todoFind->title = $request->title;
            $todoFind->description = $request->description;
            $todoFind->save();
        }
        catch(QueryException $ex){
            return response()->json([
                'status' => false,
                'msg' => $ex->getMessage()
            ], Response::HTTP_CONFLICT);
        }
        return response()->json([
            'status' => true,
            'data' => $todoFind
        ], Response::HTTP_OK);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $todoFind = UserTodo::find($id);
        if (!$todoFind) {
            return response()->json([
                'status' => false,
                'msg' => '404 Not Found',
            ], Response::HTTP_NOT_FOUND);
        }
        try {
            $todoFind->delete();
        }
        catch(QueryException $ex){
            return response()->json([
                'status' => false,
                'msg' => $ex->getMessage()
            ], Response::HTTP_CONFLICT);
        }
        return response()->json([
            'status' => true,
            'data' => "Todo Deleted successfully"
        ], Response::HTTP_OK);

    }
}
